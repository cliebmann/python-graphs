#!/usr/bin/env bash

dot -T png -O my1st.dot

dot -T ps graph1.gv -o graph1.ps
dot -T ps graph2.gv -o graph2.ps
dot -T ps graph3.gv -o graph3.ps
dot -T ps graph4.gv -o graph4.ps
dot -T ps graph5.gv -o graph5.ps
dot -T ps graph6.gv -o graph6.ps
dot -T ps graph7.gv -o graph7.ps
dot -T ps graph8.gv -o graph8.ps

dot -T png graph1.gv -o graph1.png
dot -T png graph2.gv -o graph2.png
dot -T png graph3.gv -o graph3.png
dot -T png graph4.gv -o graph4.png
dot -T png graph5.gv -o graph5.png
dot -T png graph6.gv -o graph6.png
dot -T png graph7.gv -o graph7.png
dot -T png graph8.gv -o graph8.png