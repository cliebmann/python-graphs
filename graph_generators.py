#!/usr/bin/env frameworkpython3

import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt

import random

def go():
    #G = nx.havel_hakimi_graph(3)
    #G = nx.complete_graph(4)
    #G = nx.barbell_graph(5,2)
    #G = nx.dorogovtsev_goltsev_mendes_graph(3)
    #G  = nx.ladder_graph(4)
    #G = nx.lollipop_graph(6,3)
    #G = nx.florentine_families_graph()
    G = nx.fast_gnp_random_graph(8, 0.5, directed=True)
    # ts = nx.topological_sort(DAG)
    # networkx.exception.NetworkXUnfeasible: Graph contains a cycle.
    

    nx.draw(G)
    plt.show()

def create_and_draw_balanced_tree(r, h, i):
    G = nx.balanced_tree(r, h)
    plt.figure(i)
    nx.draw(G)

def balanced_tree():
    #create_and_draw_balanced_tree(1 ,1, 1)
    #create_and_draw_balanced_tree(2, 1, 2)
    #create_and_draw_balanced_tree(1, 2, 3)
    #create_and_draw_balanced_tree(2, 2, 4)
    #create_and_draw_balanced_tree(1, 3, 5)
    #create_and_draw_balanced_tree(3, 1, 6)
    #create_and_draw_balanced_tree(2, 2, 7)
    create_and_draw_balanced_tree(2, 3, 8)

    plt.show()

def dag():
    G = nx.fast_gnp_random_graph(8, 0.5, directed=True)
    G = nx.DiGraph([(u,v,{'weight':random.randint(-10,10)}) for (u,v) in G.edges() if u<v])
    print(nx.is_directed_acyclic_graph(G))

    ts = nx.topological_sort(G)
    print(ts)

    #pos = nx.spring_layout(G)
    pos = graphviz_layout(G, prog='dot')

    nx.draw_networkx_nodes(G, pos, node_color='red') # cmap=plt.get_cmap('jet'),
    nx.draw_networkx_edges(G, pos, edge_color='black', arrows=True)

    plt.show()

#go()
#balanced_tree()
dag()