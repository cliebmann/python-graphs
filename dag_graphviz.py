#!/usr/bin/env frameworkpython3

import networkx as nx
import graphviz as gv

import random

import functools
digraph = functools.partial(gv.Digraph, format='svg')

def add_nodes(graph, nodes):
    for n in nodes:
        if isinstance(n, tuple):
            graph.node(n[0], **n[1])
        else:
            graph.node(str(n))
    return graph

def add_edges(graph, edges):
    for e in edges:
        if isinstance(e[0], tuple):
            graph.edge(*e[0], **e[1])
        else:
            graph.edge(str(e[0]), str(e[1]))
    return graph

nxg = nx.fast_gnp_random_graph(20, 0.15, directed=True)
nxg = nx.DiGraph([(u,v,{'weight':random.randint(-10,10)}) for (u,v) in nxg.edges() if u<v])

gvg = add_edges(add_nodes(digraph(), nxg.nodes()), nxg.edges())
gvg.render('dag_graphviz_4')